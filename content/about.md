---
title: About
---

Not Just Paranoid is a tech tech blog started in 2007, when it was originally called NationPigeon. It was [renamed](/nationpigeon-is-not-paranoid) on the 23rd of September 2020.

![Old NationPigeon Logo](/NationPigeonTitle.gif)

Words found on this site came from [Pete's](https://petermaynard.co.uk) head. Check out my security news feed over at <https://port22.co.uk>

My public GPG Key is below if you fancy sending me messages. 

	FEF1 FD3D CD50 3E2D E02B  EDA2 248F C016 ABB8 D69D

