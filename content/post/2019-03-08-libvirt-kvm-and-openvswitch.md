---
title: "Libvrit, KVM, and Open vSwitch"
date: 2019-03-08T10:41:49Z
---

Lab notes for configuring Libvrt, KVM, and Open vSwitch, along with a SPAN port, which mirrors all traffic on the switch to a specific port. Though keep in mind that you might not want a SPAN port, but might want to use some of the more fancy features such as ERSPAN. 

I'm writing this because I had a few hiccups when getting libvirt and OVS to play nice. This was due to me not reading OVS documentation enough, along with my stubborn desire to configure it all using Virt-Manager. I didn't find a way to do this. Since libvirt does support OVS, but the virt-manager didn't let me setup the NIC with OVS support. Anyway, here are the step so you too can reproduce this networking mess.

1. Create three VMs using virt-manager. Assign one machine with two ports, one will be used for the port mirroring.
2. Create a OVS switch:
	
		ovs-vsctl add-br testsw

2. Edit the configs so that each VM's NIC is connected to the virtual switch:

		virsh edit <vm name>

		<interface type='bridge'>
		...
		 <source bridge='testsw'/> 
		 <virtualport type='openvswitch'/>
		...
		</interface>

3. Re-start all the VMs, when they have finished initialising you can check the status of the switch:

		ovs-vsctl show

	    Bridge testsw
	        Port "vnet1"
	            Interface "vnet1" 
	        Port "vnet0"
	            Interface "vnet0"
	        Port "vnet2"
	            Interface "vnet2"
	        Port "vnet3"
	            Interface "vnet3" 
	        Port ovsbr
	            Interface ovsbr
	                type: internal
	    ovs_version: "2.9.2"

4. Creating the SPAN port, requires you to identify which port you want to use. In this case vnet3. Remove it from the switch (it was added on boot) and add it again, but as a mirror port. Note: *you need the double hyphens `--` between the commands.*
	
		ovs-vsctl del-port vnet3
		ovs-vsctl add-port ovsbr vnet3\
			 -- --id=@p get port vnet3\
			 -- --id=@m create mirror name=m0 select-all=true output-port=@p\
			 -- set bridge ovsbr mirrors=@m

5. On the VM with the span port, you'll need to bring up the interface, and you should be good to go:

		ip link set <interface> up
		tcpdump -i <interface>

# Prerequisites

## KVM and Libvirt

	apt install qemu qemu-kvm libvirt-bin bridge-utils

## Virt Manager 
	
	apt install virt-manager

## Open vSwitch

	apt install openvswitch-switch

# References

- <https://www.linuxtechi.com/install-configure-kvm-ubuntu-18-04-server/>
- <https://docs.openvswitch.org/en/latest/howto/libvirt/>
- <https://docs.openvswitch.org/en/latest/faq/configuration/>
