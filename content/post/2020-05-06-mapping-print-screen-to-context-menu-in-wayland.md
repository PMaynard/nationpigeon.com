---
title: Mapping Print Screen (PrtSc) to Context Menu (right mouse click) in Wayland/Sway
date: 2020-05-06T00:00:00Z
tags: [sway, wayland, keyboard]
---

Back in ye olden days, I used to set this in my i3 config. 

	exec_always --no-startup-id "xmodmap -e 'keycode  107 = Menu'"

Now though, I've no idea what I'm doing. This is a brain dump post, detailing how I remapped PrtSc to Menu.

# Find keycodes

	xev -event keyboard

Running the above and pressing print screen will look like this:

	KeyPress event, serial 28, synthetic NO, window 0x2400001,
	    root 0x39c, subw 0x0, time 163760698, (527,424), root:(1807,424),
	    state 0x0, keycode 107 (keysym 0xff61, Print), same_screen YES,
	    XLookupString gives 0 bytes: 
	    XmbLookupString gives 0 bytes: 
	    XFilterEvent returns: False


Now we know the keycode is 107, keysym is 0xff61, 'Print'.

# Mapping

My sway config has these lines: 

```python
	input * {
	    xkb_layout gb
	    xkb_options "altwin:prtsc_rwin"
	}
```

And I replaced `Super_R` and `RWIN` with, `Menu` and `MENU` respectively, in */usr/share/X11/xkb/symbols/altwin*. Located right at the bottom of the file.

```diff
 // Win is mapped to the PrtSc key (and the usual Win key).
 partial modifier_keys
 xkb_symbols "prtsc_rwin" {
-    replace key <PRSC> { [ Super_R, Super_R ] };
-    modifier_map Mod4 { <PRSC>, <RWIN> };
+    replace key <PRSC> { [ Menu, Menu ] };
+    modifier_map Mod4 { <PRSC>, <MENU> };
 };

```

I did try using `Menu` instead of `prtsc_rwin`, but that didn't work, I don't know why?

# fin

Yeah, I'm 100% sure there is a cleaner way to do this. And, in the future, I shall figure it out. But in the meantime, this works!

* * * 

- https://askubuntu.com/questions/991742/modifying-keyboard-layout-in-wayland
	- This one had the answer
- https://medium.com/@damko/a-simple-humble-but-comprehensive-guide-to-xkb-for-linux-6f1ad5e13450 
	- Some background on XKB. (XKB works with wayland)
- https://realh.co.uk/wp/linux-keymap-hacking/
	- Some person getting their mac working using wayland
