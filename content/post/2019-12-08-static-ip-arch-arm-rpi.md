---
title: "Static IP Address For Arch ARM (Raspberry Pi)"
date: 2019-12-08T16:31:49Z
---

This is a quick guide to set a static IP address on an Arch install for the Raspberry PI. These steps are not specific to the PI version. So feel free to use them for anything that uses SystemD and NetworkManager.

# Steps

Update currently installed software then install [NetworkManager](https://en.wikipedia.org/wiki/NetworkManager) [1](https://wiki.gnome.org/Projects/NetworkManager) [2](https://cgit.freedesktop.org/NetworkManager/NetworkManager/)

	pacman -Syu 
	pacman -S networkmanager

Enable and start the service:

	systemctrl enable NetworkManager
	systemctl start NetworkManager 

Check that it is working. You should see the status of your network.:

	nmcli 

Now list all available wireless networks the device can see. Once you've found your one, connect to it using your supplied password:

	nmcli device wifi list
	nmcli device wifi connect <SSID> password <password>

Configure it to auto connect, set static IPv4, gateway, and DNS address.

	nmcli connection modify '<SSID>' connection.autoconnect yes ipv4.method manual ipv4.address 192.168.2.199/24 ipv4.gateway 192.168.2.1 ipv4.dns 192.168.2.1

Bring the interface down and back up to make the configuration take affect.

	nmcli connection down <SSID>
	nmcli connection up <SSID>

Finally, make sure all is good - then reboot without an Ethernet cable.

	nmcli
	reboot


# Thanks goes too

- http://wiki.friendlyarm.com/wiki/index.php/Use_NetworkManager_to_configure_network_settings#Commandline_Utility_.28nmcli.29
- https://wiki.archlinux.org/index.php/NetworkManager#nmcli_examples