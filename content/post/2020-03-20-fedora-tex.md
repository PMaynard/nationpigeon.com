---
title : "Fedora and TeX: Initial Setup with Overleaf and Git"
date : "2020-03-20T15:33:49Z"
tags: [fedora, guide]
---

This is a very quick introduction to getting started with fedora and [TeX](https://en.wikipedia.org/wiki/TeX). It will cover initial installation packages, simple TeX to PDF creation, and git integration with [Overleaf](https://www.overleaf.com/), an online TeX platform.

# Base TeX

Much of my work involves working with lots of TeX, so I'm going to install all the packages I might ever need. Now that's all 1.8GB of it. You can find out about other options and what it means [here](https://web.archive.org/web/https://fedoraproject.org/wiki/Features/TeXLive).

You can search and select specific packages for your use case, but I find this is easier, since I'm often working without WiFi. Not to mention having to find and install missing TeX libraries in the middle of working on a document really messes up your train of thought.

```bash 
sudo dnf install texlive-scheme-full
```

# Build System

When using TeX you need to run three or four commands in sequence, which can change depending on the document. To save my memory I use a build system called [arara](https://gitlab.com/islandoftex/arara) which remembers and runs them for me. 

```bash 
sudo dnf install texlive-arara
```

Lines 1-4 below are the commands needed to correctly generate a pdf from this TeX source. It also calls the biber command, which sorts out the references. Also, line 5, cleans up the folder so you don't have any leftover files. 

```latex {linenos=inline}
% arara: pdflatex 
% arara: biber
% arara: pdflatex 
% arara: pdflatex 
% arara: clean: { extensions: [aux, idx, glg, ilg, bbl, ind, log, gls, glo, bcf, blg, run.xml, lof, lot, out, toc, xdy]}  

\documentclass[10pt, conference]{IEEEtran}
\usepackage{hyperref}
\hypersetup{
	hidelinks=true,
	colorlinks=true,
	urlcolor=blue
}

\usepackage[
    backend=biber,,
    url=true, 
    doi=true,
    eprint=false
]{biblatex}
\addbibresource{references.bib}

\begin{document}

Hello world

\printbibliography 
\end{document}
```

Compiling it is easy.

```bash
arara main
```

Assuming the file is called `main.tex`, if it fails you can view the error message by appending the verbose flag.  


```bash
arara main -v 
```

# Overleaf and Git

Overleaf uses username and password for authentication when using git. To auto fill these values securely, we will use gnome-keyring to cache the credentials and git will access it via [libsecrect](https://gitlab.gnome.org/GNOME/libsecret). You need to start the gnome-keyring-daemon, either [via gdm or bash](https://wiki.archlinux.org/index.php/GNOME/Keyring).

Download the libsecrect add-on for git:

```bash
sudo dnf install git-credential-libsecret
```

Then enable the credential helper for all git repositories:

```bash
git config --global credential.helper /usr/libexec/git-core/git-credential-libsecret
```

Clone the git repository from overleaf, enter your username and password (the same used for the website). This should be the last time you need to enter them as it will now be securely stored in gnome-keyring. We also rename the `origin` remote to `overleaf`, so you can add origin to another location, gitlab or github for example. This helps prevent confusion, at least for me. 

```bash
git clone https://git.overleaf.com/5635729db2vskn1gio1
git remote rename origin overleaf
```

To validate this you can use [seahorse](https://wiki.gnome.org/Apps/Seahorse) to view the stored creds, it should look something like below:

![seahorse showing the overleaf credentials](/images/2020-03-20-fedora-tex-seahourse.png)
