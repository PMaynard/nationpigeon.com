---
title: "Zotero Standalone Linux"
date: 2017-07-25T15:13:49Z
categories:
- Linux
tags:
- Linux
---

Download Zotero standalone, unzip to ```/opt/```

	sudo tar -xf Zotero-*.tar.bz2 -C /opt/

Place the following in ```~/.local/share/applications/zotero.desktop```

	#!/usr/bin/env xdg-open
	[Desktop Entry]
	Type=Application
	Name=Zotero
	GenericName=Bibliography Manager
	Icon=/opt/Zotero_linux-x86_64/chrome/icons/default/default48.png
	Exec= /opt/Zotero_linux-x86_64/zotero %f
	Categories=Office
	Terminal=false


By default zotero will place all its data in ```~/.zotero```. Knowing this you can save any highlighting and comments into the PDF document as long as its in the same location and name, zotero will sync it up.

**Updates**

This was written for zotero version 4, they have since [updated to version 5](https://www.zotero.org/blog/zotero-5-0). Which will focus on the standalone application only. The new location for data by default will be ```~/Zotero```, but if you are migrating from the firefox add-on it will be in ```~/.mozilla/firefox*```. They promised to tidy up the data folder.

**Update: 2020 March 23**

Zotero can be backed up by zipping up your profiles and data:

	tar cvfj 2020-March-23_zotero_backup-data.tar.bzip2 Zotero/
	tar cvfj 2020-March-23_zotero_backup-profile.tar.bzip2 .zotero/* 

Copy it to the new machine. You might need to change some configurations to point to the new location. e.g. if you move to a new machine with a diferrent username.

This is example, I want to replace the string `home/petemaynard/` to `home/freethink/`. This should be done everywhere which it is found. In my case was my profile's `prefs.js` and `pkcs11.txt` file.

	sed -i 's/home\/petemaynard\//home\/freethink\//g'  zotero/c7wl9rga.default/prefs.js
	sed -i 's/home\/petemaynard\//home\/freethink\//g'  zotero/c7wl9rga.default/pkcs11.txt



