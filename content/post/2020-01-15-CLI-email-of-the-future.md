---
title: "CLI Email of the Future: Office 365 and QUB"
date: 2020-01-15T10:31:49Z
tags: [guide]
---

This is a quick record of the steps taken to setup a local email copy, index, and reading. 
A list of software used is below, some are optional.

* [GetMail](http://pyropus.ca/software/getmail/) : Mail Retriever 
* [notmuch](https://notmuchmail.org/) : Local Mail Indexer 
* [alot](https://github.com/pazz/alot) : Mail User Agent (MUA)
* [w3m](https://en.wikipedia.org/wiki/W3m)* : a text based web browser and pager
* [pass](https://www.passwordstore.org/)* : The unix password manager
* [Daemontools](https://en.wikipedia.org/wiki/Daemontools)* : Process supervision toolkit 

\* Optional.


## Debian/Ubuntu

All the software should be available via apt.

	sudo apt install getmail notmuch alot w3m pass daemontools

## Fedora

Get the latest sources of getmail from the [offical site](http://pyropus.ca/software/getmail/).
Extract, build, and install: 

```
tar xvf getmail-<version>.tar.gz
cd getmail-<version>
python2 setup.py build
sudo python2 setup.py install
```

You can use the distrubtion repository for the rest. 

	sudo dnf install notmuch alot pass w3m


GetMail
-------
Talks to Office365 via IMAP over TLS. Gets all the emails and stores them locally.
Make sure to create the direcotries needed: 

```bash
mkdir -p ~/mail/staff/{cur,new,tmp}
```

*$HOME/.getmail/getmailrc*

```ini
[retriever]
type = SimpleIMAPSSLRetriever
server = outlook.office365.com
port = 993
username = <STAFF ID>@ads.qub.ac.uk
password_command = ("/usr/bin/pass", "qub/staff")
mailboxes = ALL

[destination]
type = Maildir
path = ~/mail/staff/

[options]
read_all = False
verbose = 1
message_log = ~/.getmail/staff.log
```

notmuch
-------
Indexes the local Maildir, and allows you to apply tags and search mail.

*$HOME/.notmuch-config*

```ini
[database]
path=/home/user/mail/staff

[user]
name=<My Name>
primary_email=<My Email>@qub.ac.uk

[new]
tags=unread;inbox;
ignore=

[search]
exclude_tags=deleted;spam;

[maildir]
synchronize_flags=true
```

Once you've created the config, run `notmuch new` to create the database.

alot
----
A terminal-based mail user agent (MUA) based on notmuch mail indexer.

![images/posts/2020-01-15/solarized.thread.png](/images/posts/2020-01-15/solarized.thread.png)

![/images/posts/2020-01-15/solarized.search.png](/images/posts/2020-01-15/solarized.search.png)

You'll need to setup sending accounts.

*$HOME/.config/alot/config*

```ini
[accounts]
    [[staff]]
        realname = <My Name>
        address = <My Email>@qub.ac.uk
        alias_regexp = <My Email>\+.+@qub.ac.uk
        gpg_key = <GPG Key>
        
        # TODO: Setup msmtp
        # sendmail_command = msmtp --account= -t

        # TODO: Figure out the right location.
        # sent_box = maildir:///~/mail/staff/Sent
        # draft_box = maildir://~/mail/staff/Drafts

```

mailcap
-------

Mailcap is a file the defines what should happen when a specific file type is opened in alot (or other MUAs). In this case we want to render text/HTML types using w3m. A CLI browser and web rendering engine.

*$HOME/.mailcap*

```bash
text/html;  w3m -dump -o document_charset=%{charset} '%s'; nametemplate=%s.html; copiousoutput
```

Work flow
========

Manually
--------

```bash
getmail && notmuch new && alot
```

Automated
---------

Touch `$HOME/.getmail/do-not-run` if you want to prevent getmail from running. This might be to save power or you just don't want emails coming in.
We have also used `setlock` to prevent multiple instances running at once. This is to prevent strange things from happening.

*crontab -e*
```bash
*/10 * * * * [ -f $HOME/.getmail/do-not-run ] || /usr/bin/setlock -n $HOME/.getmail/run.lock /usr/bin/getmail && notmuch new
```

Future
======
Syncing the unread/read flags via IMAP. Might need to change over to OfflineIMAP or something?

* <https://deferred.io/2016/01/18/notmuch-tags-gmail-labels-bidirectional-sync.html>

Sending email

* <https://marlam.de/msmtp/>

* * * 

- **Inital**: 2020 January 15th 
- **Updated**: 2020 March 20th
	- Added fedora steps and syntax highlighting.
	- Added basic alot account config.
