---
title: eduroam Cardiff Uni and Fedora
date: 2023-08-31T16:40:15+01:00
---

This is a rough blog post to help me remember what happened when I first connected to Cardiff's eduroam network. The tool was a pain, and didn't support Fedora. After failing to manually install the certificates I gave in and ran it, which presented even more problems. 

The tool is from Aruba (now HP) and seems to be part of [ClearPass Solution](https://www.arubanetworks.com/resource/clearpass-solution-overview/). 

## The follow the normal steps 

1. Connect to 'CU-Wireless'
2. Head to https://onboard.cardiff.ac.uk 
3. Choose 'Ubuntu (32/64bit)' 
   * This results in a shell script that runs a Qt application. 
   * It should work on most Linux distros that use [NetworkManager](https://wiki.archlinux.org/title/NetworkManager) 


## Tool's Requirement

* X11 - it won't work in wayland
* Polkit authentication agent running - e.g. lxpolkit [More Information](https://polkit.pages.freedesktop.org/polkit/polkit.8.html)

## Tool's Functions

* Extracts certificates included in the same download.
* Moves certificates to `/var/quickconnect/<date>`
* Created a new network profile at `/etc/NetworkManager/system-connections/eduroam`

## SELinux/Troubleshoot

* If can see the eduroam network connection and try to connect but it seems like nothing happens, even with a password (one auto generated). 
  * You may need to allow NetworkManager access to `/var/quickconnect/<date>/certificate.pk12`
