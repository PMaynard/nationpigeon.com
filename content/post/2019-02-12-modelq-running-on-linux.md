---
title: "Model Q running on Linux"
date: 2019-02-12T11:05:00Z
---

Steps to install and run [Visual Model Q](https://www.visualmodelq.com/download.html) on a GNU/Linux machine using wine, windows emulator.

Assuming you are using Debian, or one of its derivatives, install wine and winetricks:

	sudo apt install wine winetricks

Winetricks is needed for the Windows .Net library, and is installed like this:

	winetricks dotnet40

All that is left is to install Visual Model Q, which can be downloaded from its website:

	wine start VisualModelQVersion72Setup.msi

Once installation is complete, the installer will create a .desktop file allowing you to start the program.

![Screenshot of model q, running on Ubuntu 18.10.](/images/posts/modelq-ubuntu.png)
