---
title: "Getty: One Less Word"
date: 2020-01-16T12:18:49Z
---

Logging into a Linux machine you might meet a thing called virtual terminals ([vtty](https://en.wikipedia.org/wiki/Teleprinter)). Without going to into too much detail, this is a CLI prompt that lets you type commands when you don't have a GUI running. 

Now, back on track. When you login to a machine via the CLI, [agetty](http://man7.org/linux/man-pages/man8/agetty.8.html) is used. The provides you with a tty, and a login prompt. You might want to have it auto complete your username, because you're lazy.

This can be done by editing the `agetty@tty1.service`, if you're using [systemd](https://freedesktop.org/wiki/Software/systemd/), like the below: 

*/etc/systemd/system/getty.target.wants/getty@tty1.service*

	[Service]
	...
	ExecStart=-/sbin/agetty -n -o '<USERNAME>' %I $TERM
	...

If you start tty1 (the default one started) you'll be prompted with only the password.

Now, you'd start your GUI using a command like `startx`. It will process your `$HOME/.xsession` file, where you might have the command `exec sway`. That tells it to start [swaywm](https://swaywm.org/).

That's more words to type! 

Fear not, you can automate this by adding a few lines to `$HOME/.bash_profile`:

	if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then                                         
	        startx                                                                                 
	fi

If you do all these steps you'll only have to enter you [FDE](https://en.wikipedia.org/wiki/Disk_encryption) passphrase, and your user account password, then you'll be greeted with your nice GUI.

