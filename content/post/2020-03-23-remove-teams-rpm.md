---
title: "Remove Microsoft Teams RPM"
date: 2020-03-23T17:20:49Z
tags: [fedora, help]
---

Accidentally installed Microsoft teams and want to remove it?

	sudo rpm -e teamd libteam --nodeps

Don't use `dnf remove teamd` as it will take a load of other things unrelated.

Some other tips: 

	rpm -qa | grep team

This lists all packages installed. I couldn't find the name of teams after I installed it from .rpm file.

