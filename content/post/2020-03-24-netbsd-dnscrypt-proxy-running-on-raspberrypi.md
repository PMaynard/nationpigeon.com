--- 
title: "NetBSD: DNSCrypt Proxy Running on the RaspberryPI"
date: 2020-03-24T16:50:49Z
tags: [tips, rpi, netbsd]
---

These are my notes from cross compiling [dnscrypt-proxy](https://github.com/DNSCrypt/dnscrypt-proxy) on [NetBSD](https://www.netbsd.org/) for a raspberry pi.
It was surprisingly quick and easy. 

I did this a while back, but I found the notes today.

```bash
# wget/git clone the dnscrypt source.
# Build on normal machine.
env GOOS=netbsd GOARCH=arm GOARM=5 go build -mod vendor -ldflags="-s -w"

# copy to pi
scp dnscrypt-proxy pi:.

# create a configuration file
<create config> 

# [optional - this will need to be done once and for a fresh install of NetBSD]
PKG_PATH="http://cdn.NetBSD.org/pub/pkgsrc/packages/$(uname -s)/$(uname -m)/$(uname -r|cut -f '1 2' -d.)/All/"
export PKG_PATH

# install the rootcerts
pkg_add mozilla-rootcerts

# install them
mozilla-rootcerts install

# done.
```

# References 

- [1] https://www.thepolyglotdeveloper.com/2017/04/cross-compiling-golang-applications-raspberry-pi/
- [2] http://www.pkgsrc.org/#index1h1
