---
title: "Syncthing"
date: 2018-01-07T15:20:45Z
categories:
- Linux
---

### Happy New Year! - First post of the year.
This is a post I had written back in October/2017, but since messing up the move from jekyll to hugo I had not gotten round to posting it.

* * * 

Install syncthing as suggested by their [documentation](https://syncthing.net/). I like to use [their apt repo](https://apt.syncthing.net/) over my distribution's.

Create directories for your user, so systemd will run it as that user:

	mkdir -p   ~/.config/systemd/user

Get the latest [syncthing.service](https://github.com/syncthing/syncthing/blob/master/etc/linux-systemd/user/syncthing.service) file and place it in: 

	~/.config/systemd/user/syncthing.service

As of now (End 2017), it looks like this: 

	[Unit]
	Description=Syncthing - Open Source Continuous File Synchronization
	Documentation=man:syncthing(1)
	Wants=syncthing-inotify.service

	[Service]
	ExecStart=/usr/bin/syncthing -no-browser -no-restart -logflags=0
	Restart=on-failure
	SuccessExitStatus=3 4
	RestartForceExitStatus=3 4

	[Install]
	WantedBy=default.target

Enable and start, then check out <https://127.0.0.1:8384/>

	systemctl --user enable syncthing.service 
	systemctl --user start syncthing.service 
	systemctl --user status syncthing.service 

Also, to restart: 

	systemctl --user restart syncthing.service 

Finally, firewalls!!

	ufw allow syncthing
