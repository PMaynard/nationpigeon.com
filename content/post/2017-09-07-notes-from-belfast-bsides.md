---
date: 2017-09-07
title: Notes from Belfast BSides 2017
categories:
- Security
---

## Ain't Nobody Got Time For That: Dynamic Malware Analysis for the Overworked Analyst

Presenter: [Edmund Brumaghin](https://blogs.cisco.com/author/edmundbrumaghin)

### Software for lab 

Setting up a malware lab? Here are some tools.

- [IPFire](http://www.ipfire.org/) 
- [REMnux](https://remnux.org/)
	- [INETSim](http://www.inetsim.org/index.html) (Simulate services)
	- [FakeDNS](https://github.com/Crypt0s/FakeDns) - Might not be the one he was referring to. But you get the idea.

- [RegShot](https://sourceforge.net/projects/regshot) (Registry Snapshot, and Directory contents)
	- Able to identify some IOCs
- [ProcessHacker](http://processhacker.sourceforge.net/) (Real-time PID monitoring with colours) 
- [ProcessMonitor](https://docs.microsoft.com/en-us/sysinternals/downloads/procmon) (Filters for specific use-cases can be found online)
- [Filewatch](https://www.mcafee.com/uk/downloads/free-tools/filewatch.aspx) - Might not be this one.
- [ILSpy](http://ilspy.net/)

### Indicator of Compromise (IOCs)

Places to find some IOC/Malware.

- Twitter
- RSS 
- VirusTotal
- [Modern Honeypot Network](https://threatstream.github.io/mhn/) (MHN) 
	- Quickly deploy honeypot

### Open Source Intelligence (OSINT)

- Malware samples
	- [theZooo](http://thezoo.morirt.com/)
	- [KernelMode.info](http://www.kernelmode.info/forum/)

- Domains
	- [DarkNet Stats](https://dnstats.net/about.php)
	- CollecTor - Couldn't find a link.
-URLS 
	- [URL Query](https://urlquery.net/about)
	- [URLVoid](http://www.urlvoid.com/about-us/)

## The Path To Self-Securing Software

Presenters: [Gary Robinson](https://uk.linkedin.com/in/gary-robinson-aa56584) & [Yan Haung](http://www.csit.qub.ac.uk/Staff/StaffProfile/?school=&ns=b003c3867a38ec568cbd93c4b7c743928c3bdbee2f7cd78d8ff2913c5fa6e9b0)

- Automatic identification of attack vectors from source code.
	- Chomsky Hierarchy of grammar 
	- [Java parser](http://javaparser.org/), creates [abstract Syntax Tree](https://en.wikipedia.org/wiki/Abstract_syntax_tree) (AST), Can generate new source code
	- [ANTLR](http://www.antlr.org/) (Another tool for Language Recognition)
		- To parse any language, need to know the grammar of the language
	- Parser will output the tree into JSON format, which can then be used by other software.

- Create a Security Model 
- Create Engine to apply security model to project model (JSON from parser)
- [Patent] Detect where data is going on the network and generate and apply firewall rules.
- Currently automate security tests, but tests are manually written.
- Annotate code with the security flags. 
