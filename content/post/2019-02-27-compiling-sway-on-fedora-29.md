---
title: "Compiling Sway on Fedora 29"
date: 2019-02-27T11:05:00Z
---
Steps needed to install sawy and wlroots on Fedora 29.

Wayland-protocols needed to be installed from rawhide, since wlroots requires a newer version that what is currently in Fedora 29. If you find a bug in sway, update all the pacakges to their latest version before you report. This post is how to get a working install without running on rawhide.

# WLROOTS

Current stable packages are installed:

	dnf install git meson wlc-devel mesa-libEGL-devel mesa-libGLES-devel libdrm-devel mesa-libgbm-devel libinput-devel libxkbcommon-devel libevdev-devel pixman-devel 

Installing wayland-protocols from rawhide:

	dnf install --enablerepo rawhide --releasever=30 wayland-protocols-devel

Build and install:

	meson build && ninja -C build && sudo ninja -C build install

Adding the recently build shared library to the path:

	echo "/usr/local/lib64" > /etc/ld.so.conf.d/wlroots.conf
	ldconfig

# SWAY

Install sway's dependencies: 

	dnf install json-c-devel pcre-devel pango-devel cairo-devel gdk-pixbuf2-devel 

Build and install, telling pkg-config where to find wlroots:

	PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig meson build && ninja -C build && sudo ninja -C build install

# THINGS

Additonal pacakges that you might want:

	dnf install dmenu rofi rxvt-unicode 

Also, take a look at [swaylock](https://github.com/swaywm/swaylock) and [swayidle](https://github.com/swaywm/swayidle). 

# LOCKING

Swaylock, if build with PAM, placed the PAM file into ``/usr/local/etc/pam.d``. Which means that PAM does not see this. To have it place the file in the correct location ([Based on this closed issue](https://github.com/swaywm/sway/issues/3096)): 

	meson build -Dsysconfdir=/etc && ninja -C build && sudo ninja -C build install


