---
date: 2009-08-22 16:25:03
slug: ogg-theora
title: Ogg Theora
categories:
- Systemless
tags:
- Free
- opensource
- video
- video editing
---

The must have information on creating free and open source video codec.  This manual shows you how to playback, encode, stream and edit.  all this and with out having to pay any royalty's. 




[Theora Cookbook](http://en.flossmanuals.net/TheoraCookbook/)
