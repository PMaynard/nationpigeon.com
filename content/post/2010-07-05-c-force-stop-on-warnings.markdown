---
date: 2010-07-05 21:30:06
slug: c-force-stop-on-warnings
title: C Force stop on warnings
categories:
- Programming
- Systemless
tags:
- c
- Programming
- warning message
---

To make sure that you write good clean code, you might be wondering how to make your complier stop on warnings, well now you can with this additional command line option:

_**-Werror**_
