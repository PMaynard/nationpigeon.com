---
title: "Pigeons and Gifs"
date: 2019-10-09
---

I guess at some point in time (Maybe [2008](/new-banner) [2010](/new-design), or [2012](site-update-21-alpha)) the nation pigeon banner faded into obscurity. And remained only a fragment of lost memory up until the other day.

See, I'd recently reunited with someone from that time period, and in his classic fashion, asked me about the bobin' pigeon banner I once had.

Shock horror! 

Good point. I'd better remedy this.

![NationPigeonTitle.gif](/NationPigeonTitle.gif)

