---
title: Migrating Matrix Synapse to Another Server
date: 2020-05-29T16:00:00Z
tags: [matrix, postgresql, linux]
--- 


Its that time of year! My old synapse VM is getting old, lets move it to Fedora.
This post will just focus on database and config stuff, if you want to know my matrix is setup, [view this post](https://nationpigeon.com/homeserver-at-home/).

## Old Box

Backup original server, you probaly want to compress the SQL before moving it.

	sudo -u postgres pg_dump -C synapse | 20200529-synapse.sql
	sudo zip synapse.etc.zip /etc/synapse/* 

## Fedora box

Install and enable the basics:

	dnf install matrix-synapse python-psycopg2 postgresql-server 
	systemctl enable postgresql
	systemctl start postgresql

Initialise the database:

	sudo -u postgres initdb
	sudo -u postgres createuser synapse_user

Import the backup:

	cat 20200529-synapse.sql | sudo -u postgres psql
	cp *.yaml /etc/synapse/
	cp *.key /etc/synapse/

# Done

That should be it. Worked for me.
