---
title: Steam streaming on a headless Linux machine with Wayland
date: 2020-05-05T00:00:00Z
tags: [fedora, steam, wayland, wayvnc]
---

These are the steps taken to setup Steam streaming on a headless machine running Linux. These steps were preformed completely without any input or output devices on the headless system. Only network and power cables were plugged in.

1. Fresh install of Fedroa Server (I used 32)
2. Enable [RPM Fussion](https://rpmfusion.org/)
	- ```dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm```
3. Add gaming user 
	- ```useradd gaming```
4. Install sway, remote desktop, terminal, and steam.
	- ```dnf install sway wayvnc lxterminal steam```
5. Start headless sway, then vnc server:
	- ```WLR_BACKENDS=headless WLR_LIBINPUT_NO_DEVICES=1 sway```
	- ```wayvnc 0.0.0.0```
6. Via VNC, start steam and sign in. [Supported VNC clients](https://github.com/any1/neatvnc#client-compatibility)
7. [Optional] Shutdown steam, then copy over your steam library.
	- ```rsync -raP .local/share/Steam/ gaming@<server>:~/.local/share/Steam```

Those are all the required steps. Now on your local machine, load up Steam and you'll be presented with an option to run your games on the server.

You'll want to set the screen resolution, this can be done by adding the following into your sway configuration file on the headless machine:

```
output HEADLESS-1 resolution 1920x1080
```

Make sure to checkout the VNC server's [FAQ](https://github.com/any1/wayvnc/blob/master/FAQ.md) if your local machine is also running sway.

If you want to make sure that everything is stopped, run the following: 

```
pkill -u gaming 
```
