---
title: Project Gemini - Caster
date: 2020-05-01T00:00:00Z
tags: [fedora, gemini, caster]
---

[Project Gemini](https://gemini.circumlunar.space/) is a new protocol trying to fit in-between Gopher and HTTP. Checkout the [spec](https://gemini.circumlunar.space/docs/spec-spec.txt) and [FAQ](https://gemini.circumlunar.space/docs/faq.txt) for more details. I found a gemini client, called [caster](https://git.sr.ht/~julienxx/castor), which is written in rust and uses GTK to display the 'cap' (cap==webpage). There are other clients, however, they are heavily based on Gopher ergo commandline based.

This are some of the packages I needed to install to be able to complete caster on Fedora 32, there were probably others, but I'm sure you can figure it out - if not ping me.

	sudo dnf install cairo-gobject-devel openssl-devel pango-devel atk-devel gdk-pixbuf2-devel gtk3-devel

I kept getting an error saying it can't link, turned out to be an error with OpenSSL. Now, I know I have the development headers installed, yet, it would not work. Finally, I figured it out, the crate, [native-tls](https://lib.rs/crates/native-tls) aka [openssl](https://docs.rs/openssl/0.10.24/openssl/), did not like (needed an older version) the version Fedora had. So after some trial and error, I told rust to use a vendored version of SSL. 

```diff
diff --git a/Cargo.toml b/Cargo.toml
index 99a1492..e72f8b1 100644
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -15,7 +15,6 @@ glib-sys = "*"
 pango = "*"
 open = "*"
 regex = "*"
-native-tls = "*"
 openssl = "*"
 url = "*"
 tempfile = "*"
@@ -29,4 +28,8 @@ serde_derive="*"
 
 [dependencies.gtk]
 version = "0.8.0"
-features = ["v3_22"]
\ No newline at end of file
+features = ["v3_22"]
+
+[dependencies.native-tls]
+version = "*"
+features = ["vendored"]
```

Once it was working I decided to spice it up a bit with some colouring.

*~/.config/caster/settings.toml*
```toml
[general]
start_url = "gemini://gemini.circumlunar.space/capcom"

[colors]
h1 = "#272635"
h2 = "#6C8694"
h3 = "#8FB6C3"
list = "#272635"
text = "#272635"
background = "#E8E9F3"

[characters]
h1 = "│   "
h2 = "║  "
h3 = "║│ "
list = "▢"

[monospace]
gemini=false
gopher=true
finger=true
```

Which makes it look a bit like this: 

![caster running with the above settings](/images/posts/2020-05-01-caster.png)

Project Gemini has some nice ideas, and an active community. I'd love for [hugo](https://gohugo.io/) (site generator) to include Gemini as an output option. Moreover, the Caster (browser?) is functional, but provides only a slight upgrade from traditional commandline interfaces, perhaps this is intentional, or not. Either way, I plan to have a poke around the code, and will be keeping an eye out for any 'quick wins'. The first being a 'reading mode' and tabs. 
