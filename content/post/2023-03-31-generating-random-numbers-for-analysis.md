---
title: Generating Random Numbers for Analysis
date: 2023-03-31T11:10:15+01:00
---

The below script will use the Linux kernel's random number generator  [urandom(4)](https://linux.die.net/man/4/urandom) to generate random numbers. 

[xxd(1)](https://linux.die.net/man/1/xxd) is takes the random numbers and converts them into hex, ensuring that the correct length of 32 bytes is output per line. 

This will output 10 random numbers of length 32 byes. 

```bash
#!/bin/bash
len=32 # bytes.

for i in {1..10}
do
	xxd -l "$len" -c "$len" -p /dev/urandom >> RN_urndm.txt
done
```

Example output is shown below:

```sh
ad84753bd1c3ab28e3cfecf0407c5f16ddae68ae3f487181a172b872f0c9bad7
54bee1cbeae9bae03a6f24752e2e6f71f9644b99ef7aac6469d299bbce4d4ac0
057414982d1c8f1b5ab651bf18c39615558aa17ae5240d41649463a6352e459e
87b913ac4ddce162d78bb0ff7720ae9ba55fe6c66057f867064d6bd79ee55596
c171334bcc13ba3992c065dd8da4e67f7d92f8bf7e8302a8fd672f582c2dcb4e
848d6b85e85152785578d85ecda22152bac86a7691d544046fdcaae6ca6c5fbf
3c87e058ad3e20a43eae72c442fd1732c60a0ae7b779b54c785012f4e1a4ae50
00ff738496af8c08b92008c959c97207ebffa5b316d5479f0506e9ce6ef9b89c
d742c86c45dd330168bced4e680f388353a15bbbce56133f4b5c4cbc3c0493e9
9f715c05ecff51a55d676517b65420d5176dc2db886e7bd0c532df685c714013
```

This script may be used to generate a data set for [random number analysis](https://en.wikipedia.org/wiki/Randomness_test). 

## See also

12 years ago I posted a similar article on [Generate 1000 random words using UNIX](/generate-1000-random-words-using-unix/).
