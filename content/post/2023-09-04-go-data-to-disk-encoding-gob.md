---
title: Go Serialise Data to Disk Using encoding/gob
date: 2023-09-04T09:10:15+01:00
---

I wanted use [encoding/gob](https://pkg.go.dev/encoding/gob) to store data on disk, but couldn't find a simple example. This is my result, however, it seems to have increased in complexity a little. 

***

Serialises a structure and writes to disk as a file. The data can be read back in and accessed as a normal struct. The data stored on disk is plain text so be careful where you store it. 

First run with no existing file "./data.store"

	freethink# go run disk-gob.go
	2023/09/04 09:58:16 Could not find existing store.
	2023/09/04 09:58:16 DS.Principles Initialised.
	2023/09/04 09:58:16 Loaded 2 DS.Principles.
	2023/09/04 09:58:16 Data Store Last Updated on: 0001-01-01 00:00:00 +0000 UTC
	2023/09/04 09:58:16 example@email.com 65706574 77207361
	2023/09/04 09:58:16 another@email.com 68207265 0a650a65

Second run with an existing file "./data.store"

	freethink# go run disk-gob.go
	2023/09/04 09:59:14 Loaded 2 DS.Principles.
	2023/09/04 09:59:14 Data Store Last Updated on: 2023-09-04 09:58:16.034821023 +0100 BST
	2023/09/04 09:59:14 example@email.com 65706574 77207361
	2023/09/04 09:59:14 another@email.com 68207265 0a650a65

A hexdump of the data on disk.

	freethink# hexdump data.store -C
	00000000  32 ff 81 03 01 01 05 53  74 6f 72 65 01 ff 82 00  |2......Store....|
	00000010  01 02 01 09 55 70 64 61  74 65 64 4f 6e 01 ff 84  |....UpdatedOn...|
	00000020  00 01 0a 50 72 69 6e 63  69 70 6c 65 73 01 ff 88  |...Principles...|
	00000030  00 00 00 10 ff 83 05 01  01 04 54 69 6d 65 01 ff  |..........Time..|
	00000040  84 00 00 00 2a ff 87 04  01 01 19 6d 61 70 5b 73  |....*......map[s|
	00000050  74 72 69 6e 67 5d 6d 61  69 6e 2e 50 72 69 6e 63  |tring]main.Princ|
	00000060  69 70 6c 65 01 ff 88 00  01 0c 01 ff 86 00 00 33  |iple...........3|
	00000070  ff 85 03 01 02 ff 86 00  01 03 01 05 45 6d 61 69  |............Emai|
	00000080  6c 01 0c 00 01 0a 50 72  69 76 61 74 65 4b 65 79  |l.....PrivateKey|
	00000090  01 0c 00 01 09 50 75 62  6c 69 63 4b 65 79 01 0c  |.....PublicKey..|
	000000a0  00 00 00 74 ff 82 01 0f  01 00 00 00 0e dc 87 92  |...t............|
	000000b0  65 04 68 44 06 00 3c 01  02 06 74 65 73 74 30 31  |e.hD..<...test01|
	000000c0  01 11 65 78 61 6d 70 6c  65 40 65 6d 61 69 6c 2e  |..example@email.|
	000000d0  63 6f 6d 01 08 36 35 37  30 36 35 37 34 01 08 37  |com..65706574..7|
	000000e0  37 32 30 37 33 36 31 00  06 74 65 73 74 30 32 01  |7207361..test02.|
	000000f0  11 61 6e 6f 74 68 65 72  40 65 6d 61 69 6c 2e 63  |.another@email.c|
	00000100  6f 6d 01 08 36 38 32 30  37 32 36 35 01 08 30 61  |om..68207265..0a|
	00000110  36 35 30 61 36 35 00 00                           |650a65..|
	00000118


{{< gist pmaynard a3ae177fb3d6f39d9c32a62253227f0f >}}